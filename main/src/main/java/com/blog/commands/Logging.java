package com.blog.commands;

import org.springframework.stereotype.Component;

@Component
public class Logging implements Command {
    @Override
    public boolean supports(Object input) {
        return true;
    }

    @Override
    public void handle(Object input) {
        System.out.println("### got input : " + input);
    }
}
