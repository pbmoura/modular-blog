package com.blog.post;

import com.blog.commands.Commands;
import com.blog.domain.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostController {

    @Autowired
    Commands commands;

    @PostMapping("/post")
    public void newPost(PostData input) {
        Post post = new Post();
        post.setText(input.text);
        commands.handle(post);
    }
}
