package com.blog.commands.post;

import com.blog.commands.Command;
import com.blog.domain.Post;
import com.blog.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostSaveCommand implements Command {

    @Autowired
    private PostService service;

    @Override
    public boolean supports(Object input) {
        return input instanceof Post;
    }

    @Override
    public void handle(Object input) {
        Post post = (Post)input;
        service.save(post);
    }
}
