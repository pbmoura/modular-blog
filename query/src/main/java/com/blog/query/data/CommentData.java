package com.blog.query.data;

import java.time.LocalDateTime;

public class CommentData {
    public String text;
    public String publicationDate;
}
