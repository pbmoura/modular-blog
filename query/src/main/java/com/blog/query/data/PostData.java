package com.blog.query.data;

import java.time.LocalDateTime;
import java.util.List;

public class PostData {
    public String text;
    public String publicationDate;
    public List<CommentData> comments;
}
