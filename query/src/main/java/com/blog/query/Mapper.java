package com.blog.query;

import com.blog.domain.Comment;
import com.blog.domain.Post;
import com.blog.query.data.CommentData;
import com.blog.query.data.PostData;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class Mapper {

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd");

    public PostData mapPost(Post input) {
        PostData output = new PostData();
        output.text = input.getText();
        output.publicationDate = Optional.ofNullable(input.getPublicationDate()).map(d -> d.format(FORMATTER)).orElse("");
        output.comments = input.getComments().stream().map(this::mapComment).collect(Collectors.toList());
        return output;
    }

    public CommentData mapComment(Comment input) {
        CommentData output = new CommentData();
        output.text = input.getText();
        output.publicationDate = Optional.ofNullable(input.getPublicationDate()).map(d -> d.format(FORMATTER)).orElse("");
        return output;
    }
}
