package com.blog.query;

import com.blog.query.data.PostData;
import com.blog.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class Controller {

    @Autowired
    private PostService service;

    @Autowired
    private Mapper mapper;

    @GetMapping("/post/{id}")
    public PostData get(@PathVariable Integer id) {
        return mapper.mapPost(service.find(id));
    }

    @GetMapping("posts")
    public List<PostData> get() {
        return service.list().stream().map(mapper::mapPost).collect(Collectors.toList());
    }

}
