package com.blog.commands;

public interface Command {

    public boolean supports(Object input);
    public void handle(Object input);
}
