package com.blog.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class Commands {
    @Autowired
    List<Command> commands;

    @PostConstruct
    void listAdapters() {
        System.out.println("\n\n-------SYSTEM COMMANDS ------\n");
        for (Command command: commands) {
            System.out.println("...Loaded Class: " + command.getClass());
        }
        System.out.println("\n-------------------------------");
    }

    public void handle(Object input) {
        for(Command command : commands) {
            if (command.supports(input))
                command.handle(input);
        }
    }
}
