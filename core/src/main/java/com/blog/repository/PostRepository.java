package com.blog.repository;

import com.blog.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class PostRepository {

    @Autowired
    EntityManager entityManager;

    @Transactional
    public void save(Post post) {
        entityManager.persist(post);
    }

    public Post find(Integer id) {
        return entityManager.find(Post.class, id);
    }

    public List<Post> list() {
        return entityManager.createQuery("SELECT p FROM Post p", Post.class).getResultList();
    }
}
