package com.blog.services;

import com.blog.domain.Comment;
import com.blog.domain.Post;
import com.blog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService {

    @Autowired
    private PostRepository repository;

    public Post find(Integer id) {
        com.blog.model.Post post = repository.find(id);
        return mapPostToDomain(post);
    }

    public void save(Post post) {
        if (post.getPublicationDate() == null) {
            post.setPublicationDate(LocalDateTime.now());
        }
        repository.save(mapPostToModel(post));
    }

    private Post mapPostToDomain(com.blog.model.Post input) {
        Post output = new Post();
        output.setText(input.getText());
        output.setPublicationDate(input.getPublicationDate());
        input.getComments().forEach(comment -> output.addComment(mapComment(comment, output)));
        return output;
    }

    private com.blog.model.Post mapPostToModel(Post input) {
        com.blog.model.Post output = new com.blog.model.Post();
        output.setText(input.getText());
        output.setPublicationDate(input.getPublicationDate());
        return output;
    }

    private Comment mapComment(com.blog.model.Comment input, Post post) {
        Comment output = new Comment();
        output.setText(input.getText());
        output.setPublicationDate(input.getPublicationDate());
        output.setPost(post);
        return output;
    }

    public List<Post> list() {
        return repository.list().stream().map(this::mapPostToDomain).collect(Collectors.toList());
    }
}
